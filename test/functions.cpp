#include <gtest/gtest.h>
#include <src/functions.h>

TEST(Func, BinaryNegate)
{
    cutil::binary_negate<int, std::less<int> > n;
    ASSERT_FALSE(n(0, 1));
    ASSERT_TRUE(n(0, 0));
    ASSERT_TRUE(n(1, 0));
}

#include <gtest/gtest.h>
#include <src/binary-tree-functions.h>

namespace {

    struct node {
        node* parent;
        node* left;
        node* right;

        node(node* p, node* l, node* r)
            : parent(p)
            , left(l)
            , right(r)
        {}

        explicit node(node* p)
            : parent(p)
            , left(NULL)
            , right(NULL)
        {}
    };

    struct tree {
        node root;
        node l;
        node r;
        node l_l;
        node l_r;
        node r_l;
        node r_r;

        tree()
            : root(NULL, &l, &r)
            , l(&root, &l_l, &l_r)
            , r(&root, &r_l, &r_r)
            , l_l(&l)
            , l_r(&l)
            , r_l(&r)
            , r_r(&r)
        {}
    };

}

TEST(BinaryTreeFuncs, DefaultTraitsReadOnly)
{
    cutil::bt::adv_traits<cutil::bt::default_basic_traits<node> > traits;
    tree const t;
    ASSERT_TRUE(traits.is_left(t.l));
    ASSERT_TRUE(traits.is_left(t.l_l));
    ASSERT_TRUE(traits.is_left(t.r_l));

    ASSERT_FALSE(traits.is_left(t.r));
    ASSERT_FALSE(traits.is_left(t.l_r));
    ASSERT_FALSE(traits.is_left(t.r_r));

    ASSERT_FALSE(traits.is_left(traits.uncle(t.l_l)));
    ASSERT_FALSE(traits.is_left(traits.uncle(t.l_r)));
    ASSERT_TRUE(traits.is_left(traits.uncle(t.r_l)));
    ASSERT_TRUE(traits.is_left(traits.uncle(t.r_r)));

    ASSERT_EQ(&t.r, &traits.sibling(t.l));
    ASSERT_EQ(&t.l, &traits.sibling(t.r));
    ASSERT_EQ(&t.l_r, &traits.sibling(t.l_l));
    ASSERT_EQ(&t.l_l, &traits.sibling(t.l_r));
    ASSERT_EQ(&t.r_r, &traits.sibling(t.r_l));
    ASSERT_EQ(&t.r_l, &traits.sibling(t.r_r));
}

TEST(BinaryTreeFuncs, DefaultTraitsRotating)
{
    cutil::bt::adv_traits<cutil::bt::default_basic_traits<node> > traits;
    tree t;
    traits.rotate_left(t.r_r);
    ASSERT_EQ(NULL, t.root.parent);
    ASSERT_EQ(&t.l, t.root.left);
    ASSERT_EQ(&t.r_r, t.root.right);

    ASSERT_EQ(&t.root, t.l.parent);
    ASSERT_EQ(&t.l_l, t.l.left);
    ASSERT_EQ(&t.l_r, t.l.right);

    ASSERT_EQ(&t.l, t.l_l.parent);
    ASSERT_EQ(NULL, t.l_l.left);
    ASSERT_EQ(NULL, t.l_l.right);

    ASSERT_EQ(&t.l, t.l_r.parent);
    ASSERT_EQ(NULL, t.l_r.left);
    ASSERT_EQ(NULL, t.l_r.right);

    ASSERT_EQ(&t.r_r, t.r.parent);
    ASSERT_EQ(&t.r_l, t.r.left);
    ASSERT_EQ(NULL, t.r.right);

    ASSERT_EQ(&t.r, t.r_l.parent);
    ASSERT_EQ(NULL, t.r_l.left);
    ASSERT_EQ(NULL, t.r_l.right);

    ASSERT_EQ(&t.root, t.r_r.parent);
    ASSERT_EQ(&t.r, t.r_r.left);
    ASSERT_EQ(NULL, t.r_r.right);

    traits.rotate_right(t.r_l);
    ASSERT_EQ(NULL, t.root.parent);
    ASSERT_EQ(&t.l, t.root.left);
    ASSERT_EQ(&t.r_r, t.root.right);

    ASSERT_EQ(&t.root, t.l.parent);
    ASSERT_EQ(&t.l_l, t.l.left);
    ASSERT_EQ(&t.l_r, t.l.right);

    ASSERT_EQ(&t.l, t.l_l.parent);
    ASSERT_EQ(NULL, t.l_l.left);
    ASSERT_EQ(NULL, t.l_l.right);

    ASSERT_EQ(&t.l, t.l_r.parent);
    ASSERT_EQ(NULL, t.l_r.left);
    ASSERT_EQ(NULL, t.l_r.right);

    ASSERT_EQ(&t.r_l, t.r.parent);
    ASSERT_EQ(NULL, t.r.left);
    ASSERT_EQ(NULL, t.r.right);

    ASSERT_EQ(&t.r_r, t.r_l.parent);
    ASSERT_EQ(NULL, t.r_l.left);
    ASSERT_EQ(&t.r, t.r_l.right);

    ASSERT_EQ(&t.root, t.r_r.parent);
    ASSERT_EQ(&t.r_l, t.r_r.left);
    ASSERT_EQ(NULL, t.r_r.right);

    traits.rotate_right(t.r_l);
    ASSERT_EQ(NULL, t.root.parent);
    ASSERT_EQ(&t.l, t.root.left);
    ASSERT_EQ(&t.r_l, t.root.right);

    ASSERT_EQ(&t.root, t.l.parent);
    ASSERT_EQ(&t.l_l, t.l.left);
    ASSERT_EQ(&t.l_r, t.l.right);

    ASSERT_EQ(&t.l, t.l_l.parent);
    ASSERT_EQ(NULL, t.l_l.left);
    ASSERT_EQ(NULL, t.l_l.right);

    ASSERT_EQ(&t.l, t.l_r.parent);
    ASSERT_EQ(NULL, t.l_r.left);
    ASSERT_EQ(NULL, t.l_r.right);

    ASSERT_EQ(&t.r_r, t.r.parent);
    ASSERT_EQ(NULL, t.r.left);
    ASSERT_EQ(NULL, t.r.right);

    ASSERT_EQ(&t.root, t.r_l.parent);
    ASSERT_EQ(NULL, t.r_l.left);
    ASSERT_EQ(&t.r_r, t.r_l.right);

    ASSERT_EQ(&t.r_l, t.r_r.parent);
    ASSERT_EQ(&t.r, t.r_r.left);
    ASSERT_EQ(NULL, t.r_r.right);

    traits.rotate_left(t.r_r);
    ASSERT_EQ(NULL, t.root.parent);
    ASSERT_EQ(&t.l, t.root.left);
    ASSERT_EQ(&t.r_r, t.root.right);

    ASSERT_EQ(&t.root, t.l.parent);
    ASSERT_EQ(&t.l_l, t.l.left);
    ASSERT_EQ(&t.l_r, t.l.right);

    ASSERT_EQ(&t.l, t.l_l.parent);
    ASSERT_EQ(NULL, t.l_l.left);
    ASSERT_EQ(NULL, t.l_l.right);

    ASSERT_EQ(&t.l, t.l_r.parent);
    ASSERT_EQ(NULL, t.l_r.left);
    ASSERT_EQ(NULL, t.l_r.right);

    ASSERT_EQ(&t.r_l, t.r.parent);
    ASSERT_EQ(NULL, t.r.left);
    ASSERT_EQ(NULL, t.r.right);

    ASSERT_EQ(&t.r_r, t.r_l.parent);
    ASSERT_EQ(NULL, t.r_l.left);
    ASSERT_EQ(&t.r, t.r_l.right);

    ASSERT_EQ(&t.root, t.r_r.parent);
    ASSERT_EQ(&t.r_l, t.r_r.left);
    ASSERT_EQ(NULL, t.r_r.right);
}

TEST(BinaryTreeFuncs, LeftMost)
{
    cutil::bt::adv_traits<cutil::bt::default_basic_traits<node> > traits;
    tree const t;
    ASSERT_EQ(&t.l_l, &traits.left_most(t.root));
    ASSERT_EQ(&t.l_l, &traits.left_most(t.l));
    ASSERT_EQ(&t.r_l, &traits.left_most(t.r));
    ASSERT_EQ(&t.l_l, &traits.left_most(t.l_l));
    ASSERT_EQ(&t.l_r, &traits.left_most(t.l_r));
    ASSERT_EQ(&t.r_l, &traits.left_most(t.r_l));
    ASSERT_EQ(&t.r_r, &traits.left_most(t.r_r));
}

TEST(BinaryTreeFuncs, RightMost)
{
    cutil::bt::adv_traits<cutil::bt::default_basic_traits<node> > traits;
    tree const t;
    ASSERT_EQ(&t.r_r, &traits.right_most(t.root));
    ASSERT_EQ(&t.l_r, &traits.right_most(t.l));
    ASSERT_EQ(&t.r_r, &traits.right_most(t.r));
    ASSERT_EQ(&t.l_l, &traits.right_most(t.l_l));
    ASSERT_EQ(&t.l_r, &traits.right_most(t.l_r));
    ASSERT_EQ(&t.r_l, &traits.right_most(t.r_l));
    ASSERT_EQ(&t.r_r, &traits.right_most(t.r_r));
}

TEST(BinaryTreeFuncs, SwapRight)
{
    cutil::bt::adv_traits<cutil::bt::default_basic_traits<node> > traits;
    tree t;
    traits.swap_right(t.l);
    ASSERT_EQ(NULL, t.root.parent);
    ASSERT_EQ(&t.l_r, t.root.left);
    ASSERT_EQ(&t.r, t.root.right);

    ASSERT_EQ(&t.l_r, t.l.parent);
    ASSERT_EQ(NULL, t.l.left);
    ASSERT_EQ(NULL, t.l.right);

    ASSERT_EQ(&t.l_r, t.l_l.parent);
    ASSERT_EQ(NULL, t.l_l.left);
    ASSERT_EQ(NULL, t.l_l.right);

    ASSERT_EQ(&t.root, t.l_r.parent);
    ASSERT_EQ(&t.l_l, t.l_r.left);
    ASSERT_EQ(&t.l, t.l_r.right);

    ASSERT_EQ(&t.root, t.r.parent);
    ASSERT_EQ(&t.r_l, t.r.left);
    ASSERT_EQ(&t.r_r, t.r.right);

    ASSERT_EQ(&t.r, t.r_l.parent);
    ASSERT_EQ(NULL, t.r_l.left);
    ASSERT_EQ(NULL, t.r_l.right);

    ASSERT_EQ(&t.r, t.r_r.parent);
    ASSERT_EQ(NULL, t.r_r.left);
    ASSERT_EQ(NULL, t.r_r.right);
}

TEST(BinaryTreeFuncs, SwapUnrelated)
{
    cutil::bt::adv_traits<cutil::bt::default_basic_traits<node> > traits;
    tree t;
    traits.swap_unrelated(t.l_l, t.r_l);
    ASSERT_EQ(NULL, t.root.parent);
    ASSERT_EQ(&t.l, t.root.left);
    ASSERT_EQ(&t.r, t.root.right);

    ASSERT_EQ(&t.root, t.l.parent);
    ASSERT_EQ(&t.r_l, t.l.left);
    ASSERT_EQ(&t.l_r, t.l.right);

    ASSERT_EQ(&t.r, t.l_l.parent);
    ASSERT_EQ(NULL, t.l_l.left);
    ASSERT_EQ(NULL, t.l_l.right);

    ASSERT_EQ(&t.l, t.l_r.parent);
    ASSERT_EQ(NULL, t.l_r.left);
    ASSERT_EQ(NULL, t.l_r.right);

    ASSERT_EQ(&t.root, t.r.parent);
    ASSERT_EQ(&t.l_l, t.r.left);
    ASSERT_EQ(&t.r_r, t.r.right);

    ASSERT_EQ(&t.l, t.r_l.parent);
    ASSERT_EQ(NULL, t.r_l.left);
    ASSERT_EQ(NULL, t.r_l.right);

    ASSERT_EQ(&t.r, t.r_r.parent);
    ASSERT_EQ(NULL, t.r_r.left);
    ASSERT_EQ(NULL, t.r_r.right);
}

TEST(BinaryTreeFuncs, SwapNextAtRight)
{
    cutil::bt::adv_traits<cutil::bt::default_basic_traits<node> > traits;
    tree t;
    node entry(NULL, NULL, &t.root);
    t.root.parent = &entry;
    ASSERT_EQ(&t.r_l, &traits.swap_next_at_right(t.root));
    ASSERT_EQ(&t.r_l, entry.right);

    ASSERT_EQ(&t.r, t.root.parent);
    ASSERT_EQ(NULL, t.root.left);
    ASSERT_EQ(NULL, t.root.right);

    ASSERT_EQ(&t.r_l, t.l.parent);
    ASSERT_EQ(&t.l_l, t.l.left);
    ASSERT_EQ(&t.l_r, t.l.right);

    ASSERT_EQ(&t.l, t.l_l.parent);
    ASSERT_EQ(NULL, t.l_l.left);
    ASSERT_EQ(NULL, t.l_l.right);

    ASSERT_EQ(&t.l, t.l_r.parent);
    ASSERT_EQ(NULL, t.l_r.left);
    ASSERT_EQ(NULL, t.l_r.right);

    ASSERT_EQ(&t.r_l, t.r.parent);
    ASSERT_EQ(&t.root, t.r.left);
    ASSERT_EQ(&t.r_r, t.r.right);

    ASSERT_EQ(&entry, t.r_l.parent);
    ASSERT_EQ(&t.l, t.r_l.left);
    ASSERT_EQ(&t.r, t.r_l.right);

    ASSERT_EQ(&t.r, t.r_r.parent);
    ASSERT_EQ(NULL, t.r_r.left);
    ASSERT_EQ(NULL, t.r_r.right);

    ASSERT_EQ(&t.l_r, &traits.swap_next_at_right(t.l));
    ASSERT_EQ(&t.r_l, entry.right);

    ASSERT_EQ(&t.r, t.root.parent);
    ASSERT_EQ(NULL, t.root.left);
    ASSERT_EQ(NULL, t.root.right);

    ASSERT_EQ(&t.l_r, t.l.parent);
    ASSERT_EQ(NULL, t.l.left);
    ASSERT_EQ(NULL, t.l.right);

    ASSERT_EQ(&t.l_r, t.l_l.parent);
    ASSERT_EQ(NULL, t.l_l.left);
    ASSERT_EQ(NULL, t.l_l.right);

    ASSERT_EQ(&t.r_l, t.l_r.parent);
    ASSERT_EQ(&t.l_l, t.l_r.left);
    ASSERT_EQ(&t.l, t.l_r.right);

    ASSERT_EQ(&t.r_l, t.r.parent);
    ASSERT_EQ(&t.root, t.r.left);
    ASSERT_EQ(&t.r_r, t.r.right);

    ASSERT_EQ(&entry, t.r_l.parent);
    ASSERT_EQ(&t.l_r, t.r_l.left);
    ASSERT_EQ(&t.r, t.r_l.right);

    ASSERT_EQ(&t.r, t.r_r.parent);
    ASSERT_EQ(NULL, t.r_r.left);
    ASSERT_EQ(NULL, t.r_r.right);
}

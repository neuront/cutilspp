#include <gtest/gtest.h>
#include <src/kth-heap.h>
#include <algorithm>

TEST(KthHeap, AsMinHeap)
{
    cutil::kth_string_heap<int, 0, std::less<int> > int_min_heap;
    int_min_heap.push(0);
    ASSERT_EQ(0, int_min_heap.top());
    ASSERT_EQ(1, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());
    ASSERT_EQ(0, int_min_heap.pop());
    ASSERT_EQ(0, int_min_heap.size());
    ASSERT_TRUE(int_min_heap.empty());

    int_min_heap.push(1);
    ASSERT_EQ(1, int_min_heap.top());
    ASSERT_EQ(1, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(6);
    ASSERT_EQ(1, int_min_heap.top());
    ASSERT_EQ(2, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(7);
    ASSERT_EQ(1, int_min_heap.top());
    ASSERT_EQ(3, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(0);
    ASSERT_EQ(0, int_min_heap.top());
    ASSERT_EQ(4, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(0, int_min_heap.pop());
    ASSERT_EQ(3, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(1, int_min_heap.pop());
    ASSERT_EQ(2, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(2);
    ASSERT_EQ(2, int_min_heap.top());
    ASSERT_EQ(3, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(2, int_min_heap.pop());
    ASSERT_EQ(2, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(9);
    ASSERT_EQ(6, int_min_heap.top());
    ASSERT_EQ(3, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(6, int_min_heap.pop());
    ASSERT_EQ(2, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(7, int_min_heap.pop());
    ASSERT_EQ(1, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(9, int_min_heap.pop());
    ASSERT_EQ(0, int_min_heap.size());
    ASSERT_TRUE(int_min_heap.empty());
}

TEST(KthHeap, As3rdHeap)
{
    cutil::kth_string_heap<int, 2, std::less<int> > int_min_heap;
    int_min_heap.push(1);
    ASSERT_EQ(1, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());
    int_min_heap.push(6);
    ASSERT_EQ(2, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());
    int_min_heap.push(7);
    ASSERT_EQ(3, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());
    ASSERT_EQ(7, int_min_heap.top());
    ASSERT_EQ(7, int_min_heap.pop());
    ASSERT_EQ(2, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(1);
    ASSERT_EQ(6, int_min_heap.top());
    ASSERT_EQ(3, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(6);
    ASSERT_EQ(6, int_min_heap.top());
    ASSERT_EQ(4, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(7);
    ASSERT_EQ(6, int_min_heap.top());
    ASSERT_EQ(5, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(0);
    ASSERT_EQ(1, int_min_heap.top());
    ASSERT_EQ(6, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(1, int_min_heap.pop());
    ASSERT_EQ(5, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(6, int_min_heap.pop());
    ASSERT_EQ(4, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(2);
    ASSERT_EQ(2, int_min_heap.top());
    ASSERT_EQ(5, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(2, int_min_heap.pop());
    ASSERT_EQ(4, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    int_min_heap.push(9);
    ASSERT_EQ(6, int_min_heap.top());
    ASSERT_EQ(5, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(6, int_min_heap.pop());
    ASSERT_EQ(4, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(7, int_min_heap.pop());
    ASSERT_EQ(3, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());

    ASSERT_EQ(9, int_min_heap.pop());
    ASSERT_EQ(2, int_min_heap.size());
    ASSERT_FALSE(int_min_heap.empty());
}

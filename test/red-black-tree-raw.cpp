#include <gtest/gtest.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <src/binary-tree-functions.h>
#include <src/red-black-tree.h>

namespace {

    struct node {
        node* parent;
        node* left;
        node* right;
        bool is_black;
        int value;

        node()
            : parent(NULL)
            , left(NULL)
            , right(NULL)
            , is_black(false)
            , value(0)
        {}

        node& reset()
        {
            return reset(0);
        }

        node& reset(int v)
        {
            left = NULL;
            right = NULL;
            parent = NULL;
            is_black = false;
            value = v;
            return *this;
        }

        node& set_value(int v)
        {
            value = v;
            return *this;
        }
    };

    typedef cutil::bt::adv_traits<cutil::bt::default_basic_traits<node> > bt_traits;
    typedef cutil::rbtree_raw<cutil::default_rbnode_traits<bt_traits> > rbtree;
    typedef bt_traits::node_cref node_cref;
    typedef bt_traits::node_ref node_ref;

    struct info_value {
        std::string const info;
        std::vector<int> value_trace;

        info_value(std::string const& i, int v)
            : info(i)
        {
            value_trace.push_back(v);
        }

        void print_trace() const
        {
            std::cerr << info << std::endl;
            for (std::vector<int>::const_iterator begin = value_trace.begin();
                 begin != value_trace.end();
                 ++begin)
            {
                std::cerr << "  At value=" << *begin << std::endl;
            }
        }
    };

    int verify_node(node_cref node, node_cref parent)
    {
        try {
            if (&parent != node.parent) {
                std::stringstream ss;
                ss << "Expected parent: " << &parent << std::endl;
                ss << "  Actual parent: " << node.parent << std::endl;
                throw ss.str();
            }
            if (!node.is_black && !parent.is_black) {
                throw std::string("Color");
            }
            int left_black_count = node.left == NULL ? 0 : verify_node(*node.left, node);
            int right_black_count = node.right == NULL ? 0 : verify_node(*node.right, node);
            if (left_black_count != right_black_count) {
                std::stringstream ss;
                ss << " Left black count: " << left_black_count << std::endl;
                ss << "Right black count: " << right_black_count << std::endl;
                throw ss.str();
            }
            if (node.left != NULL && node.value < node.left->value) {
                std::stringstream ss;
                ss << "Left value: " << node.left->value << std::endl;
                ss << "This value: " << node.value << std::endl;
                throw ss.str();
            }
            if (node.right != NULL && node.right->value < node.value) {
                std::stringstream ss;
                ss << "Right value: " << node.right->value << std::endl;
                ss << " This value: " << node.value << std::endl;
                throw ss.str();
            }
            return left_black_count + (node.is_black ? 1 : 0);
        } catch (std::string const& info) {
            throw info_value(info, node.value);
        } catch (info_value& e) {
            e.value_trace.push_back(node.value);
            throw e;
        }
    }

    bool verify_tree(rbtree const& tree)
    {
        node_cref entry = tree.enter();
        if (!entry.is_black) {
            std::cerr << "entry not black" << std::endl;
            return false;
        }
        if (entry.left != NULL) {
            std::cerr << "entry.left not NULL" << std::endl;
            return false;
        }
        if (entry.parent == NULL) {
            if (entry.right != NULL) {
                std::cerr << "entry.right not NULL" << std::endl;
                return false;
            }
            return true;
        }
        try {
            verify_node(*entry.parent, entry);
            return true;
        } catch (info_value const& iv) {
            iv.print_trace();
            return false;
        }
    }

}

TEST(RedBlackTreeRaw, InsertLeftRight)
{
    node entry;
    entry.is_black = true;
    cutil::default_rbnode_traits<bt_traits> tr;
    rbtree tree(entry.set_value(0), tr);
    ASSERT_TRUE(verify_tree(tree));

    std::vector<node> nodes(20);
    tree.insert_root(nodes[0].set_value(20));
    ASSERT_TRUE(verify_tree(tree));

    node_ref root = *tree.enter().parent;
    tree.insert_left(root, nodes[1].set_value(19));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(root, nodes[2].set_value(21));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(tr.left_most(*entry.right), nodes[3].set_value(18));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(tr.left_most(*entry.right), nodes[4].set_value(17));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(tr.left_most(*entry.right), nodes[5].set_value(16));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(tr.left_most(*entry.right), nodes[6].set_value(15));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(tr.left_most(*entry.right), nodes[7].set_value(14));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(tr.left_most(*entry.right), nodes[8].set_value(13));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(tr.left_most(*entry.right), nodes[9].set_value(12));
    ASSERT_TRUE(verify_tree(tree));

    tree.insert_right(tr.right_most(*entry.right), nodes[10].set_value(22));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[11].set_value(23));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[12].set_value(24));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[13].set_value(25));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[14].set_value(26));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[15].set_value(27));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[16].set_value(28));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[17].set_value(29));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[18].set_value(30));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[19].set_value(31));
    ASSERT_TRUE(verify_tree(tree));
}

TEST(RedBlackTreeRaw, RandomInsertLeft)
{
    node entry;
    entry.is_black = true;
    cutil::default_rbnode_traits<bt_traits> tr;
    rbtree tree(entry.set_value(0), tr);
    ASSERT_TRUE(verify_tree(tree));

    std::vector<node> nodes(4);
    tree.insert_root(nodes[0].set_value(4));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(nodes[0], nodes[1].set_value(2));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(nodes[1], nodes[2].set_value(3));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(tr.left_most(*entry.right), nodes[3].set_value(1));
    ASSERT_TRUE(verify_tree(tree));
}

TEST(RedBlackTreeRaw, RandomInsertRight)
{
    node entry;
    entry.is_black = true;
    cutil::default_rbnode_traits<bt_traits> tr;
    rbtree tree(entry.set_value(0), tr);
    ASSERT_TRUE(verify_tree(tree));

    std::vector<node> nodes(4);
    tree.insert_root(nodes[0].set_value(4));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(nodes[0], nodes[1].set_value(6));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_left(nodes[1], nodes[2].set_value(5));
    ASSERT_TRUE(verify_tree(tree));
    tree.insert_right(tr.right_most(*entry.right), nodes[3].set_value(7));
    ASSERT_TRUE(verify_tree(tree));
}

TEST(RedBlackTreeRaw, SwapNextAtRight)
{
    node entry;
    entry.is_black = true;
    cutil::default_rbnode_traits<bt_traits> tr;
    rbtree tree(entry, tr);

    std::vector<node> nodes(4);
    tree.insert_root(nodes[0]);
    tree.insert_right(nodes[0], nodes[1]);
    ASSERT_EQ(entry.right, &nodes[0]);
    tr.swap_next_at_right(nodes[0]);
    ASSERT_FALSE(nodes[0].is_black);
    ASSERT_EQ(&nodes[1], nodes[0].parent);
    ASSERT_TRUE(nodes[1].is_black);
    ASSERT_EQ(&entry, nodes[1].parent);
    tr.swap_next_at_right(nodes[1]);
    ASSERT_TRUE(nodes[0].is_black);
    ASSERT_EQ(&entry, nodes[0].parent);
    ASSERT_FALSE(nodes[1].is_black);
    ASSERT_EQ(&nodes[0], nodes[1].parent);

    tree.insert_left(nodes[0], nodes[2]);
    tree.insert_left(nodes[1], nodes[3]);
    ASSERT_EQ(nodes[1].left, &nodes[3]);
    ASSERT_EQ(nodes[0].right, &nodes[1]);
    ASSERT_EQ(entry.right, &nodes[0]);
    tr.swap_next_at_right(nodes[0]);
    ASSERT_FALSE(nodes[0].is_black);
    ASSERT_EQ(&nodes[1], nodes[0].parent);
    ASSERT_TRUE(nodes[3].is_black);
    ASSERT_EQ(&entry, nodes[3].parent);
}

TEST(RedBlackTreeRaw, RemoveRoot)
{
    node entry;
    entry.is_black = true;
    cutil::default_rbnode_traits<bt_traits> tr;
    rbtree tree(entry, tr);
    std::vector<node> nodes(7);
    tree.insert_root(nodes[0]);
    tree.remove(nodes[0]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.remove(nodes[1]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.remove(nodes[1]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.remove(nodes[1]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    nodes[0].is_black = true;
    nodes[1].is_black = true;
    nodes[2].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[1]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[3].reset(3));
    nodes[0].is_black = true;
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    tree.insert_left(nodes[3], nodes[2].reset(2));
    tree.insert_right(nodes[3], nodes[4].reset(4));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[1]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[3].reset(3));
    nodes[0].is_black = true;
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    tree.insert_right(nodes[3], nodes[4].reset(4));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[1]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[3].reset(3));
    nodes[0].is_black = true;
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    tree.insert_left(nodes[3], nodes[2].reset(2));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[1]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[3].reset(3));
    nodes[0].is_black = true;
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    tree.insert_left(nodes[3], nodes[2].reset(2));
    tree.insert_right(nodes[3], nodes[4].reset(4));
    nodes[3].is_black = false;
    nodes[2].is_black = true;
    nodes[4].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[1]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[4].reset(4));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[4].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[3]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[4].reset(4));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[4].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    nodes[1].is_black = false;
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[3]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[4].reset(4));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[4].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[3]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[4].reset(4));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[4].is_black = true;
    tree.insert_right(nodes[1], nodes[2].reset(2));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[3]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[3]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    nodes[4].is_black = true;
    nodes[6].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[3]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    nodes[1].is_black = false;
    nodes[5].is_black = false;
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    nodes[4].is_black = true;
    nodes[6].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[3]);
    ASSERT_TRUE(verify_tree(tree));
}

TEST(RedBlackTreeRaw, RemoveTerminal)
{
    node entry;
    entry.is_black = true;
    cutil::default_rbnode_traits<bt_traits> tr;
    rbtree tree(entry, tr);
    std::vector<node> nodes(7);

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[0]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[2]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[0]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[1].reset(1));
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[2]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[0]);
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[2]);
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[4]);
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[6]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    nodes[5].is_black = false;
    nodes[4].is_black = true;
    nodes[6].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[1]);

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    nodes[1].is_black = false;
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[5]);

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    nodes[4].is_black = true;
    nodes[6].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[0]);
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[2]);
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[4]);
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[6]);
    ASSERT_TRUE(verify_tree(tree));

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    nodes[1].is_black = false;
    nodes[5].is_black = false;
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    nodes[4].is_black = true;
    nodes[6].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[0]);
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[2]);
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[4]);
    ASSERT_TRUE(verify_tree(tree));
    tree.remove(nodes[6]);
    ASSERT_TRUE(verify_tree(tree));
}

TEST(RedBlackTreeRaw, RandomRemove)
{
    node entry;
    entry.is_black = true;
    cutil::default_rbnode_traits<bt_traits> tr;
    rbtree tree(entry, tr);
    std::vector<node> nodes(7);

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    nodes[1].is_black = false;
    nodes[5].is_black = false;
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    nodes[4].is_black = true;
    nodes[6].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    for (std::vector<node>::iterator i = nodes.begin(); i != nodes.end(); ++i) {
        tree.remove(*i);
        ASSERT_TRUE(verify_tree(tree)) << i->value;
    }

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    nodes[1].is_black = true;
    nodes[5].is_black = false;
    nodes[0].is_black = false;
    nodes[2].is_black = false;
    nodes[4].is_black = true;
    nodes[6].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    for (std::vector<node>::iterator i = nodes.begin(); i != nodes.end(); ++i) {
        tree.remove(*i);
        ASSERT_TRUE(verify_tree(tree)) << i->value;
    }

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    nodes[1].is_black = false;
    nodes[5].is_black = false;
    nodes[0].is_black = true;
    nodes[2].is_black = true;
    nodes[4].is_black = true;
    nodes[6].is_black = true;
    for (std::vector<node>::reverse_iterator i = nodes.rbegin(); i != nodes.rend(); ++i) {
        tree.remove(*i);
        ASSERT_TRUE(verify_tree(tree)) << i->value;
    }

    entry.reset(-1);
    entry.is_black = true;
    tree.insert_root(nodes[3].reset(3));
    tree.insert_left(nodes[3], nodes[1].reset(1));
    tree.insert_right(nodes[3], nodes[5].reset(5));
    nodes[1].is_black = true;
    nodes[3].is_black = true;
    nodes[5].is_black = true;
    tree.insert_left(nodes[1], nodes[0].reset(0));
    tree.insert_right(nodes[1], nodes[2].reset(2));
    tree.insert_left(nodes[5], nodes[4].reset(4));
    tree.insert_right(nodes[5], nodes[6].reset(6));
    nodes[1].is_black = true;
    nodes[5].is_black = false;
    nodes[0].is_black = false;
    nodes[2].is_black = false;
    nodes[4].is_black = true;
    nodes[6].is_black = true;
    ASSERT_TRUE(verify_tree(tree));
    for (std::vector<node>::reverse_iterator i = nodes.rbegin(); i != nodes.rend(); ++i) {
        tree.remove(*i);
        ASSERT_TRUE(verify_tree(tree)) << i->value;
    }
}

#ifndef __C_UTILS_PP_REFERENCE_H__
#define __C_UTILS_PP_REFERENCE_H__

namespace cutil {

    template <typename _Reference>
    bool null(_Reference const& r)
    {
        return &r == NULL;
    }

    template <typename _Type>
    _Type& null()
    {
        return *(_Type*)0;
    }

}

#endif /* __C_UTILS_PP_REFERENCE_H__ */

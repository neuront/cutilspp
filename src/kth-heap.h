#ifndef __C_UTILS_PP_KTH_HEAP_H__
#define __C_UTILS_PP_KTH_HEAP_H__

#include <cassert>
#include <queue>
#include <vector>
#include "functions.h"

namespace cutil {

    template <typename _Tp, unsigned _K, typename _Compare>
    class kth_string_heap {
        std::priority_queue<_Tp, std::vector<_Tp>, _Compare> lessers;
        std::priority_queue<_Tp, std::vector<_Tp>, binary_negate<_Tp, _Compare> > greaters;
    public:
        void push(_Tp const& value)
        {
            lessers.push(value);
            if (_K < lessers.size()) {
                greaters.push(lessers.top());
                lessers.pop();
            }
        }

        _Tp pop()
        {
            assert (!greaters.empty());
            _Tp result(greaters.top());
            greaters.pop();
            return result;
        }

        _Tp const& top() const
        {
            assert (!greaters.empty());
            return greaters.top();
        }

        int size() const
        {
            return int(lessers.size() + greaters.size());
        }

        bool empty() const
        {
            return lessers.empty();
        }
    };

    template <typename _Tp, typename _Compare>
    class kth_string_heap<_Tp, 0, _Compare>
    {
        std::priority_queue<_Tp, std::vector<_Tp>, binary_negate<_Tp, _Compare> > heap;
    public:
        void push(_Tp const& value)
        {
            heap.push(value);
        }

        _Tp pop()
        {
            assert (!heap.empty());
            _Tp result(heap.top());
            heap.pop();
            return result;
        }

        _Tp const& top() const
        {
            assert (!heap.empty());
            return heap.top();
        }

        int size() const
        {
            return int(heap.size());
        }

        bool empty() const
        {
            return heap.empty();
        }
    };

}

#endif /* __C_UTILS_PP_KTH_HEAP_H__ */

#ifndef __C_UTILS_PP_FUNCTIONS_H__
#define __C_UTILS_PP_FUNCTIONS_H__

namespace cutil {

    template <typename _Tp, typename _CompareBinaryFn>
    struct binary_negate {
        _CompareBinaryFn fn;

        binary_negate()
            : fn(_CompareBinaryFn())
        {}

        explicit binary_negate(_CompareBinaryFn const& f)
            : fn(f)
        {}

        bool operator()(_Tp const& lhs, _Tp const& rhs)
        {
            return !fn(lhs, rhs);
        }
    };

}

#endif /* __C_UTILS_PP_FUNCTIONS_H__ */

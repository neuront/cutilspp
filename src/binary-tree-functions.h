#ifndef __C_UTILS_PP_BINARY_TREE_FUNCTIONS_H__
#define __C_UTILS_PP_BINARY_TREE_FUNCTIONS_H__

#include <cassert>
#include "reference.h"

namespace cutil { namespace bt {

    template <typename _NodeType>
    struct default_basic_traits {
        typedef _NodeType node_type;
        typedef node_type& node_ref;
        typedef node_type const& node_cref;

        node_ref parent(node_ref node)
        {
            assert (!null(node));
            return *node.parent;
        }

        node_cref parent(node_cref node)
        {
            assert (!null(node));
            return *node.parent;
        }

        void set_parent(node_ref node, node_ref parent)
        {
            if (!null(node)) {
                node.parent = &parent;
            }
        }

        node_ref left(node_ref node)
        {
            assert (!null(node));
            return *node.left;
        }

        node_cref left(node_cref node)
        {
            assert (!null(node));
            return *node.left;
        }

        void set_left(node_ref node, node_ref left)
        {
            assert (!null(node));
            node.left = &left;
        }

        node_ref right(node_ref node)
        {
            assert (!null(node));
            return *node.right;
        }

        node_cref right(node_cref node)
        {
            assert (!null(node));
            return *node.right;
        }

        void set_right(node_ref node, node_ref right)
        {
            assert (!null(node));
            node.right = &right;
        }
    };

    template <typename _BasicTraits>
    struct adv_traits
        : public _BasicTraits
    {
        typedef typename _BasicTraits::node_type node_type;
        typedef typename _BasicTraits::node_ref node_ref;
        typedef typename _BasicTraits::node_cref node_cref;

        node_ref grandparent(node_ref node)
        {
            assert (!null(node));
            return this->parent(this->parent(node));
        }

        node_cref grandparent(node_cref node)
        {
            assert (!null(node));
            return this->parent(this->parent(node));
        }

        node_ref uncle(node_ref node)
        {
            assert (!null(node));
            return is_left(this->parent(node)) ? this->right(grandparent(node))
                                               : this->left(grandparent(node));
        }

        node_cref uncle(node_cref node)
        {
            assert (!null(node));
            return is_left(this->parent(node)) ? this->right(grandparent(node))
                                               : this->left(grandparent(node));
        }

        node_ref sibling(node_ref node)
        {
            assert (!null(node));
            return is_left(node) ? this->right(this->parent(node)) : this->left(this->parent(node));
        }

        node_cref sibling(node_cref node)
        {
            assert (!null(node));
            return is_left(node) ? this->right(this->parent(node)) : this->left(this->parent(node));
        }

        bool is_left(node_cref node)
        {
            return &(this->left(this->parent(node))) == &node;
        }

        node_ref left_most(node_ref node)
        {
            assert (!null(node));
            return null(this->left(node)) ? node : left_most(this->left(node));
        }

        node_cref left_most(node_cref node)
        {
            assert (!null(node));
            return null(this->left(node)) ? node : left_most(this->left(node));
        }

        node_ref right_most(node_ref node)
        {
            assert (!null(node));
            return null(this->right(node)) ? node : right_most(this->right(node));
        }

        node_cref right_most(node_cref node)
        {
            assert (!null(node));
            return null(this->right(node)) ? node : right_most(this->right(node));
        }

        void replace_child(node_ref origin, node_ref rep)
        {
            assert (!null(origin));
            node_ref parent = this->parent(origin);
            is_left(origin) ? this->set_left(parent, rep) : this->set_right(parent, rep);
        }

        void rotate_left(node_ref pivot)
        {
            assert (!null(pivot) && !null(this->parent(pivot)));
            assert (&pivot == &this->right(this->parent(pivot)));
            this->set_right(this->parent(pivot), this->left(pivot));
            this->set_parent(this->left(pivot), this->parent(pivot));
            this->set_left(pivot, this->parent(pivot));
            this->set_parent(pivot, grandparent(pivot));
            replace_child(this->left(pivot), pivot);
            this->set_parent(this->left(pivot), pivot);
        }

        void rotate_right(node_ref pivot)
        {
            assert (!null(pivot) && !null(this->parent(pivot)));
            assert (&pivot == &this->left(this->parent(pivot)));
            this->set_left(this->parent(pivot), this->right(pivot));
            this->set_parent(this->right(pivot), this->parent(pivot));
            this->set_right(pivot, this->parent(pivot));
            this->set_parent(pivot, grandparent(pivot));
            replace_child(this->right(pivot), pivot);
            this->set_parent(this->right(pivot), pivot);
        }

        void swap_right(node_ref node)
        {
            assert (!null(this->right(node)));
            node_ref left = this->left(node);
            node_ref parent = this->parent(node);
            node_ref right = this->right(node);
            node_ref new_left = this->left(right);
            node_ref new_right = this->right(right);
            this->set_parent(right, parent);
            replace_child(node, right);
            this->set_left(right, left);
            this->set_parent(left, right);
            this->set_right(right, node);
            this->set_parent(node, right);
            this->set_left(node, new_left);
            this->set_parent(new_left, node);
            this->set_right(node, new_right);
            this->set_parent(new_right, node);
        }

        void swap_unrelated(node_ref a, node_ref b)
        {
            assert (&a != &b);
            assert (&this->parent(a) != &this->parent(b));
            assert (&a != &this->parent(b) && &b != &this->parent(a));
            node_ref a_left = this->left(a);
            node_ref a_right = this->right(a);
            node_ref a_parent = this->parent(a);
            node_ref b_left = this->left(b);
            node_ref b_right = this->right(b);
            node_ref b_parent = this->parent(b);
            this->set_left(a, b_left);
            this->set_parent(b_left, a);
            this->set_left(b, a_left);
            this->set_parent(a_left, b);
            this->set_right(a, b_right);
            this->set_parent(b_right, a);
            this->set_right(b, a_right);
            this->set_parent(a_right, b);
            replace_child(a, b);
            replace_child(b, a);
            this->set_parent(b, a_parent);
            this->set_parent(a, b_parent);
        }

        node_ref swap_next_at_right(node_ref node)
        {
            assert (!null(node) && !null(this->right(node)));
            node_ref right_left = this->left(this->right(node));
            if (null(right_left)) {
                node_ref right = this->right(node);
                swap_right(node);
                return right;
            }
            node_ref next = left_most(right_left);
            swap_unrelated(node, next);
            return next;
        }
    };

} }

#endif /* __C_UTILS_PP_BINARY_TREE_FUNCTIONS_H__ */

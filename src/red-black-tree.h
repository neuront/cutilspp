#ifndef __C_UTILS_PP_RED_BLACK_TREE_H__
#define __C_UTILS_PP_RED_BLACK_TREE_H__

#include <cassert>
#include "reference.h"

namespace cutil {

    template <typename _Traits>
    struct default_rbnode_traits
        : public _Traits
    {
        typedef typename _Traits::node_type node_type;
        typedef typename _Traits::node_ref node_ref;
        typedef typename _Traits::node_cref node_cref;

        bool is_black(node_cref node)
        {
            return node.is_black;
        }

        bool is_black_or_leaf(node_cref node)
        {
            return null(node) || node.is_black;
        }

        void set_black(node_ref node)
        {
            set_black(node, true);
        }

        void set_black(node_ref node, bool black)
        {
            node.is_black = black;
        }

        void set_red(node_ref node)
        {
            set_black(node, false);
        }

        void swap_color(node_ref a, node_ref b)
        {
            bool a_black = is_black(a);
            set_black(a, is_black(b));
            set_black(b, a_black);
        }

        node_ref swap_next_at_right(node_ref node)
        {
            node_ref next = _Traits::swap_next_at_right(node);
            swap_color(node, next);
            return next;
        }
    };

    template <typename _Traits>
    class rbtree_raw {
        typedef typename _Traits::node_type node_type;
        typedef typename _Traits::node_ref node_ref;
        typedef typename _Traits::node_cref node_cref;

        node_ref entry;
        _Traits traits;

        void insert_fix_node(node_ref node)
        {
            if (traits.is_black(traits.parent(node))) {
                return;
            }
            if (!null(traits.uncle(node)) && !traits.is_black(traits.uncle(node))) {
                traits.set_black(traits.parent(node));
                traits.set_black(traits.uncle(node));
                traits.set_red(traits.grandparent(node));
                insert_fix_node(traits.grandparent(node));
                return;
            }
            if (traits.is_left(traits.parent(node))) {
                if (!traits.is_left(node)) {
                    traits.rotate_left(node);
                    insert_fix_as_left(node);
                } else {
                    insert_fix_as_left(traits.parent(node));
                }
            } else {
                if (traits.is_left(node)) {
                    traits.rotate_right(node);
                    insert_fix_as_right(node);
                } else {
                    insert_fix_as_right(traits.parent(node));
                }
            }
        }

        void insert_fix_as_left(node_ref node)
        {
            traits.rotate_right(node);
            traits.set_black(node);
            traits.set_red(traits.right(node));
        }

        void insert_fix_as_right(node_ref node)
        {
            traits.rotate_left(node);
            traits.set_black(node);
            traits.set_red(traits.left(node));
        }

        void remove_node(node_ref node)
        {
            assert (!null(node));
            if (null(traits.left(node))) {
                if (null(traits.right(node))) {
                    if (is_root(node)) {
                        traits.set_parent(entry, null<node_type>());
                        traits.set_right(entry, null<node_type>());
                        return;
                    }
                    node_ref sibling = traits.sibling(node);
                    traits.replace_child(node, null<node_type>());
                    if (traits.is_black(node)) {
                        remove_fix(traits.parent(node), sibling);
                    }
                    return;
                }
                node_ref parent = traits.parent(node);
                node_ref right = traits.right(node);
                node_ref sibling = traits.sibling(node);
                traits.replace_child(node, right);
                traits.set_parent(right, parent);
                if (traits.is_black(node)) {
                    remove_fix(parent, sibling);
                }
            } else if (null(traits.right(node))) {
                node_ref parent = traits.parent(node);
                node_ref left = traits.left(node);
                node_ref sibling = traits.sibling(node);
                traits.replace_child(node, left);
                traits.set_parent(left, parent);
                if (traits.is_black(node)) {
                    remove_fix(parent, sibling);
                }
            } else {
                traits.swap_next_at_right(node);
                node_ref parent = traits.parent(node);
                node_ref right = traits.right(node);
                node_ref sibling = traits.sibling(node);
                traits.replace_child(node, right);
                traits.set_parent(right, parent);
                if (traits.is_black(node)) {
                    remove_fix(parent, sibling);
                }
            }
        }

        void remove_fix(node_ref parent, node_ref sibling)
        {
            if (null(sibling)) {
                if (!traits.is_black(parent)) {
                    traits.set_black(parent);
                    return;
                }
                remove_fix(traits.parent(parent), traits.sibling(parent));
                return;
            }
            if (!traits.is_black(sibling)) {
                traits.set_black(sibling);
                traits.set_red(parent);
                if (traits.is_left(sibling)) {
                    traits.rotate_right(sibling);
                    remove_fix(parent, traits.left(parent));
                } else {
                    traits.rotate_left(sibling);
                    remove_fix(parent, traits.right(parent));
                }
                return;
            }
            bool sibling_left_is_black_or_leaf = traits.is_black_or_leaf(traits.left(sibling));
            bool sibling_right_is_black_or_leaf = traits.is_black_or_leaf(traits.right(sibling));
            if (sibling_left_is_black_or_leaf && sibling_right_is_black_or_leaf) {
                traits.set_red(sibling);
                if (traits.is_black(parent)) {
                    remove_fix(traits.parent(parent), traits.sibling(parent));
                } else {
                    traits.set_black(parent);
                }
                return;
            }
            if (!sibling_left_is_black_or_leaf && !sibling_right_is_black_or_leaf) {
                traits.set_black(traits.left(sibling));
                traits.set_black(traits.right(sibling));
                traits.set_red(parent);
                if (traits.is_left(sibling)) {
                    traits.rotate_right(sibling);
                    remove_fix(parent, traits.left(parent));
                } else {
                    traits.rotate_left(sibling);
                    remove_fix(parent, traits.right(parent));
                }
                return;
            }
            if (!sibling_left_is_black_or_leaf) {
                if (traits.is_left(sibling)) {
                    remove_fix_on_left(parent, sibling);
                } else {
                    traits.set_black(traits.left(sibling));
                    traits.set_red(sibling);
                    traits.rotate_right(traits.left(sibling));
                    remove_fix_on_right(parent, traits.parent(sibling));
                }
            }
            if (!sibling_right_is_black_or_leaf) {
                if (traits.is_left(sibling)) {
                    traits.set_black(traits.right(sibling));
                    traits.set_red(sibling);
                    traits.rotate_left(traits.right(sibling));
                    remove_fix_on_left(parent, traits.parent(sibling));
                } else {
                    remove_fix_on_right(parent, sibling);
                }
            }
        }

        void remove_fix_set_sibling_color(node_ref parent, node_ref sibling, node_ref nephew)
        {
            traits.set_black(sibling, traits.is_black(parent));
            traits.set_black(parent);
            traits.set_black(nephew);
        }

        void remove_fix_on_left(node_ref parent, node_ref sibling)
        {
            remove_fix_set_sibling_color(parent, sibling, traits.left(sibling));
            traits.rotate_right(sibling);
        }

        void remove_fix_on_right(node_ref parent, node_ref sibling)
        {
            remove_fix_set_sibling_color(parent, sibling, traits.right(sibling));
            traits.rotate_left(sibling);
        }

        bool is_root(node_cref node) const
        {
            return entry.right == &node;
        }
    public:
        explicit rbtree_raw(node_ref e)
            : entry(e)
        {
            assert (null(traits.parent(e)) && null(traits.left(e)) && null(traits.right(e)));
            assert (traits.is_black(e));
        }

        rbtree_raw(node_ref e, _Traits const& t)
            : entry(e)
            , traits(t)
        {
            assert (null(traits.parent(e)) && null(traits.left(e)) && null(traits.right(e)));
            assert (traits.is_black(e));
        }

        node_ref enter()
        {
            return entry;
        }

        node_cref enter() const
        {
            return entry;
        }

        void insert_root(node_ref root)
        {
            assert (!null(root));
            assert (null(traits.parent(entry)));
            assert (null(traits.left(entry)) && null(traits.right(entry)));
            assert (null(traits.left(root)) && null(traits.right(root)));
            traits.set_parent(entry, root);
            traits.set_right(entry, root);
            traits.set_parent(root, entry);
            traits.set_black(root);
        }

        void insert_left(node_ref slot, node_ref inserter)
        {
            assert (!null(slot) && !null(inserter));
            assert (null(traits.left(slot)));
            assert (null(traits.left(inserter)) && null(traits.left(inserter)));
            assert (!traits.is_black(inserter));
            traits.set_left(slot, inserter);
            traits.set_parent(inserter, slot);
            insert_fix_node(inserter);
            traits.set_parent(entry, traits.right(entry));
            traits.set_black(traits.parent(entry));
        }

        void insert_right(node_ref slot, node_ref inserter)
        {
            assert (!null(slot) && !null(inserter));
            assert (null(traits.right(slot)));
            assert (null(traits.left(inserter)) && null(traits.left(inserter)));
            assert (!traits.is_black(inserter));
            traits.set_right(slot, inserter);
            traits.set_parent(inserter, slot);
            insert_fix_node(inserter);
            traits.set_parent(entry, traits.right(entry));
            traits.set_black(traits.parent(entry));
        }

        void remove(node_ref node)
        {
            traits.set_red(entry);
            remove_node(node);
            traits.set_parent(entry, traits.right(entry));
            traits.set_black(entry);
        }
    };

}

#endif /* __C_UTILS_PP_RED_BLACK_TREE_H__ */

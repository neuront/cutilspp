CPLR=clang++
CC=$(CPLR) -c
INCLUDE=-I.
RESOLVE_DEP=$(CPLR) -MM $(INCLUDE)
CFLAGS=-Wall -Wextra -Wold-style-cast -Werror $(OPT_FLAGS)
MKTMP=tmp.mk
COMPILE=$(CC) $(CFLAGS) $(INCLUDE)
TESTDIR=test
TEST_EXTERN_LIBS=-lgtest -lgtest_main -lpthread
LINK=$(CPLR)

%.d:$(TESTDIR)/%.cpp
	echo -n "$(TESTDIR)/" > $(MKTMP)
	$(RESOLVE_DEP) $< >> $(MKTMP)
	echo "	$(COMPILE) $< -o $(TESTDIR)/$*.o" >> $(MKTMP)
	make -f $(MKTMP)

check: functions.d \
       binary-tree-functions.d \
       red-black-tree-raw.d \
       kth-heap.d
	$(LINK) \
	    $(TESTDIR)/functions.o \
	    $(TESTDIR)/binary-tree-functions.o \
	    $(TESTDIR)/red-black-tree-raw.o \
	    $(TESTDIR)/kth-heap.o \
	    $(TEST_EXTERN_LIBS) -o test.out
	./test.out

clean:
	rm -f $(TESTDIR)/*.o
	rm -f *.out
	rm -f $(MKTMP)
